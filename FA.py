import bpy

class MCN_PT_FA(bpy.types.Panel):
    """"""
    bl_label = "FA"
    bl_idname = "MCN_PT_FA"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "Tool"
    bl_parent_id = "MCN_PT_Main_Panel"
    bl_options = {'DEFAULT_CLOSED'}

    def draw(self, context):
        layout = self.layout

        # TODO Replace this label by your stuff here
        layout.label(text="Ne clique pas !")
        
        layout.operator("object.clique_objects")
        
        
class NeCliquePas_OT(bpy.types.Operator):

    bl_idname = "object.clique_objects"
    bl_label = "clique pas"  # nom apparaissant dans l'interface
    bl_options = {'REGISTER', 'UNDO'}
    
    def execute(self, context):
        print("Salut BG")
        
        return {'FINISHED'}


def register():
    bpy.utils.register_class(MCN_PT_FA)
    bpy.utils.register_class(NeCliquePas_OT)


def unregister():
    bpy.utils.unregister_class(MCN_PT_FA)
    bpy.utils.unregister_class(NeCliquePas_OT)
